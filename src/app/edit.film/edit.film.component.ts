import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { FilmObject } from '../FilmObject';

@Component({
  selector: 'app-edit.film',
  templateUrl: './edit.film.component.html',
  styleUrls: ['./edit.film.component.scss']
})
export class EditFilmComponent implements OnInit  {
  film : FilmObject | undefined ;
  filmForm!: FormGroup;
  constructor(public dialogRef: MatDialogRef<EditFilmComponent>, private fb: FormBuilder) { } 
  ngOnInit(): void {
    this.buildFilmForm();
    this.patchFormWithheaderInfo(this.film)
    console.log(this.film)
  }
  onNoClick(): void {
    this.dialogRef.close(this.film);
  }
close(){
  this.dialogRef.close(this.film);
}
update(){
  let updatedFilm = this.filmForm.getRawValue();
  this.dialogRef.close(updatedFilm);
}

buildFilmForm() {
  this.filmForm = this.fb.group({
    tconst: [''],
    titleType: [''],
    primaryTitle: [''],
    originalTitle: [''],
    startYear: [''],
    endYear: [''],
    runtimeMinutes: [''],
    isAdult: [''],
    genres: ['']
})
};


patchFormWithheaderInfo(value : any) {
  this.filmForm.patchValue({
    tconst: value.tconst,
    titleType: value.titleType,
    primaryTitle: value.primaryTitle,
    originalTitle : value.originalTitle,
    startYear: value.startYear,
    endYear: value.endYear,
    runtimeMinutes: value.runtimeMinutes,
    isAdult: value.isAdult,
    genres: value.genres

  });
}

}
