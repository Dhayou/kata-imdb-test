import { HttpClient } from '@angular/common/http';
import { Parser } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Papa } from 'ngx-papaparse';
import { FilmObject } from '../FilmObject';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  urlFilm = "https://datasets.imdbws.com/title.basics.tsv.gz";
path = "assets/data.tsv"
dataList : FilmObject[] = [];
  constructor(private http : HttpClient, private papa: Papa) { }



 
 

   getData(){
    const responseType: 'json' | 'arraybuffer' | 'blob' | 'text' = 'json';
     return   this.http.get(this.urlFilm,{responseType : responseType})
  }



  downloadFile(filename: string) {
    return this.http.get(this.urlFilm + 'download?filename=' + filename, {
      responseType: 'arraybuffer'
    });
  }

}
