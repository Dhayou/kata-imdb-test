export interface FilmObject {
    tconst : string,
    titleType : string,
    originalTitle : String,
    primaryTitle : String
    startYear : string,
    endYear : string,
    runtimeMinutes : string,
    isAdult : boolean,
    genres : string[]
}

