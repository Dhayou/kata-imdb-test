import { Parser } from '@angular/compiler';
import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Papa } from 'ngx-papaparse';
import { map } from 'rxjs';
import { EditFilmComponent } from './edit.film/edit.film.component';
import { FilmObject } from './FilmObject';
import { FilmService } from './services/film.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  constructor(
    private service: FilmService,
    private papa: Papa,
    public dialog: MatDialog
  ) {}
  ngAfterViewInit(): void {}
  urlFilm = 'https://datasets.imdbws.com/title.basics.tsv.gz';
  path = 'assets/data.tsv';
  dataList: FilmObject[] = [];
  film: FilmObject | undefined;
  ngOnInit(): void {
    this.papa.parse(this.path, {
      download: true,
      header: true,
      preview: 5000,
      chunk: (row) => {
        row.data.forEach((element: FilmObject) => {
          this.dataList.push(element as FilmObject);
        });
      },
      complete: () => {},
    });
    //this.service.downloadFile("data.tsv").pipe(map(response => JSON.stringify(response).slice(0,10))).subscribe(response=> console.log(response) );
  }

  openDialog(index: number): void {
    const dialogRef = this.dialog.open(EditFilmComponent, {
      width: '400px',
    });
    if (this.dataList[index]['endYear'].includes('N')) {
      this.dataList[index]['endYear'] = '';
    }
    dialogRef.componentInstance.film = this.dataList[index];
    dialogRef.afterClosed().subscribe((result) => {
      if (result != undefined || result != null) {
        this.dataList[index] = result;
      }
    });
  }

  title = 'kata-jems-test';
  term = '';
  currentIndexValue = 1;
  startIndex = 0;
  endIndex = 21;
  elementPerPage = 21 ;
  paginationArray: number[] = [1, 2, 3, 4, 5];

  getArray() {
    return this.paginationArray;
  }

  getDataWithPageNumber(pageNumber: number) {
    this.startIndex = (pageNumber-1) * this.elementPerPage;
    this.endIndex = this.startIndex + this.elementPerPage;
    this.currentPageNumber();
  }

  GetNextPagination() {
    this.paginationArray.forEach((element, index) => {
      element = element + 5;
      this.paginationArray[index] = element;
    });
    this.getDataWithPageNumber(this.paginationArray[0]);
    return this.paginationArray;
  }

  currentPageNumber() {
    return (this.currentIndexValue = this.endIndex / this.elementPerPage);
  }
  // prevIndex(length: number){
  //   this.startIndex = length * 0;
  //   console.log(this.startIndex)
  // }
  nextIndex(endIndex: number) {
    this.startIndex = this.endIndex;
    this.endIndex = this.endIndex + 21;
    this.currentPageNumber();
    if(this.currentPageNumber()- this.paginationArray[0] === 5 ){
      this.GetNextPagination()
    }
  }

  showScroll: boolean | undefined;
  showScrollHeight = 300;
  hideScrollHeight = 10;
  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      (window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop) > this.showScrollHeight
    ) {
      this.showScroll = true;
    } else if (
      this.showScroll &&
      (window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop) < this.hideScrollHeight
    ) {
      this.showScroll = false;
    }
  }

  scrollToTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
});
  }
}
